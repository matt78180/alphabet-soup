

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 

//The x and y values for each direction (diagonal, horizontal, vertical)
//Directions are: up left, left, down left, up, down, up right, right, down right
int x[] = { -1, -1, -1, 0, 0, 1, 1, 1 }; 
int y[] = { -1, 0, 1, -1, 1, -1, 0, 1 }; 

//Global Variables
int numRows;
int numCols; 


int puzzleScan(char grid[numRows][numCols], int row, int col, char *word, int *endRow, int *endCol) 
{ 

    //If starting point doesn't match first letter
    if (grid[row][col] != word[0]) 
    {
    	return 0; 
  	}
  	
    int len = strlen(word);
    int i;
  
    // Search in each of the 8 directions individually 
    for (i = 0; i < 8; i++) 
    { 
        //Increment by the x and y values specified by index i 
        int k;
        int rowDir = row + x[i];
        int colDir = col + y[i]; 
  
        // Start from second char since first character is already checked, 
        for (k = 1; k < len; k++) 
        { 
            // If out of bound break 
            if (rowDir >= numRows || rowDir < 0 || colDir >= numCols || colDir < 0) 
                break; 
  
            // If char doesn't match word break 
            if (grid[rowDir][colDir] != word[k]) 
                break; 
  
            //  Moving in particular direction 
            rowDir += x[i];
            colDir += y[i]; 
        } 
  
        // If all character matched, then value of must 
        // be equal to length of word 
        if (k == len) 
        {
        	*endRow = rowDir;
        	*endCol = colDir;
            return 1; 
        }
    } 
    return 0; 
}

void wordSearch(char grid[numRows][numCols], char* word) 
{ 
    // Consider every point as starting point and search 
    // given word 
    int row,col, eRow, eColumn;
    for (row = 0; row < numRows; row++)
    { 
       	for (col = 0; col < numCols; col++) 
       	{
        	if (puzzleScan(grid, row, col, word, &eRow, &eColumn) == 1) 
            	printf("%s %d:%d %d:%d \n" , word, row, col, eRow, eColumn);
        }
    }
}



int main()
{

	char filename[80];
	
	printf("Enter the filename : \t"); 
    scanf("%s", filename);
	//Open file
    FILE *file = fopen(filename, "r");
    
    if(file == NULL)
    {
    	printf("Error, file not found");
    	return 0;
    }
    
    //get number of rows
    char a = getc(file);
    
    //move over the 'x'
    getc(file);
    
    //get number of columns
    char b = getc(file);
    
    //convert from char to int
    numRows = a - '0';
    numCols = b - '0';
    
    
    char puzzle[numRows][numCols];
    char buffer[numRows*numCols];
  
    
	char c = getc(file);
	
    //convert file to 2D array
    for(int m = 0; m < numCols; m++)
    {
    	for(int n = 0; n < numRows; n++)
    	{
    		puzzle[n][m] = getc(file);
    		c = getc(file);
    	}
    }
    
    bzero(buffer, sizeof(buffer));
    while (fscanf(file, "%s ", buffer)==1)
    {
   		wordSearch(puzzle, buffer); 
   	}   
    
    return 0;

}

